import {Component, OnInit} from '@angular/core';
import {TaskStorageService} from "../task-storage.service";
import {Task} from "../shared/models/task.model";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  tasks: Task[];
  mode = "";
  constructor(private storage: TaskStorageService) {
    this.storage.init();
    this.tasks = this.storage.getTasks(); 
    this.loadmode();
  }

  /**
   * Load tasks on init
   */
  ngOnInit() : void{
    this.storage.init();
    this.tasks = this.storage.getTasks(); 
    this.loadmode();
  }
  
  loadmode(){
    this.tasks.length < 4 ? this.mode="F" :  //Froid
      this.tasks.length < 8 ? this.mode="A" :  //Ambient
      this.mode="C" ;  //Chaud
  }
  /**
   * Remove the tasks from the list
   *
   * @param id task index to remove
   */
  delete(id: number): void {
    this.storage.delete(id);
    this.tasks = this.storage.getTasks();
    this.loadmode();
  }
}
